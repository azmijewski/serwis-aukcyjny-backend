Sposób uruchamiania aplikacji:
1. Pobieramy repo
2. W katalogu resources tworzymy plik application-local.yml i kopiujemy zawartość application-local.example.yml
3. W utworzonym pliku wpisujemy swoje dane (dostęp do bazy itd...)
4. Ustawiamy profil na local (możemy przy każdym uruchamianiu, ale ja polecam ustawic w intelliju w konfiguracji)
5. Startujemy aplikacji

Zrobię póżniej jakiś obraz dockera tak żeby nie było zabawy z propertisami